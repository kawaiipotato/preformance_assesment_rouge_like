using UnityEngine;
using System.Collections;

public class Player : MovingObject {
	
	public float speed = 1.0f;
	private Animator animator;
	
	//private Rigidbody rb;
	
	public  void Start ()
	{
		base.Start ();
		Update ();
	}

	protected override void HandleCollision<T>(T component)
	{

	}
	
	 void Update ()
	{

	//	var move = new Vector3 (Input.GetAxis ("Horizontal"), Input.GetAxis ("Vertical"), 0);
	//	transform.position += move * speed * Time.deltaTime;
		Debug.Log ("hey");
		if (Input.GetKey (KeyCode.UpArrow)) {
			Debug.Log (" up");
			transform.Translate (Vector2.up * speed * Time.deltaTime);
			//animator.SetTrigger("jump");

		} else if (Input.GetKey (KeyCode.LeftArrow)) {
			Debug.Log (" left");
			transform.Translate (-Vector2.right * speed * Time.deltaTime);	
		} else if (Input.GetKey (KeyCode.RightArrow)) {
			Debug.Log (" right");
			transform.Translate (Vector2.right * speed * Time.deltaTime);
		} else if (Input.GetKey (KeyCode.DownArrow)) {
			Debug.Log ("down");
			transform.Translate (Vector2.down * speed * Time.deltaTime);
		}
	}

	private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith){
		if(objectPlayerCollidedWith.tag == "coin"){
			objectPlayerCollidedWith.gameObject.SetActive(false);
		} 
	}
	                          
}
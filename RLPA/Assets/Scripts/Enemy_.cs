using UnityEngine;
using System.Collections;

public class Enemy_ : MovingObject  {
	
	private bool dirRight = true;
	public float speed = 2.0f;

	protected override void HandleCollision<T>(T component)
	{
		
	}
	
	void Update () {


		if (dirRight)
			transform.Translate (Vector2.right * speed * Time.deltaTime);
		else
			transform.Translate (-Vector2.right * speed * Time.deltaTime);
		
		if(transform.position.x >= 100.0f) {
			dirRight = false;
		}
		
		if(transform.position.x <= -4) {
			dirRight = true;
		}
	}
}